(function($) {
  const refreshTimeoutInMS = 5000;

  let currentQuestion = [];
  let isPaused = false;
  let $question;
  let $pauseButton;
  let questionData;

  /**
   * Changes the question to a new random question.
   */
  function changeQuestion() {
    const currentItem =
      questionData[Math.floor(Math.random() * questionData.length)];
    currentQuestion = currentItem.question;
    $question.html(currentQuestion);
  }

  /**
   * Toggles play/paused state for the page.
   */
  function stateHandler() {
    isPaused = !isPaused;
    $pauseButton.html(isPaused ? 'Play' : 'Paused');
  }

  // Main initialization function happens at $document.ready()
  function init() {
    $pauseButton = $('#pause-button');
    $question = $('#question');

    // register click handlers
    $pauseButton.on('click', stateHandler);

    // Get Questions Data
    $.getJSON('data/questions.json', function(data) {
      questionData = data;
      changeQuestion();
    });
  }

  // DOM onready handler
  $().ready(init);

  // interval to cycle through questions - NOTE: no reason to add complexity of pausing/tracking interval -- instead just use flag for now
  setInterval(function() {
    if (!isPaused) {
      changeQuestion();
    }
  }, refreshTimeoutInMS);
})(jQuery);
